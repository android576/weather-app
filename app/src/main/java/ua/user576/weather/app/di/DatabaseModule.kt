package ua.user576.weather.app.di

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import ua.user576.weather.app.room.CitiesDatabase
import ua.user576.weather.app.room.CityDao
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class DatabaseModule {
    @Provides
    @Singleton
    fun providesDatabase(@ApplicationContext context: Context): CitiesDatabase =
        Room.databaseBuilder(context, CitiesDatabase::class.java, "cities")
            .build()

    @Provides
    fun providesDao(citiesDatabase: CitiesDatabase): CityDao =
        citiesDatabase.cityDao()
}
