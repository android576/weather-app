package ua.user576.weather.app.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class City(
    @PrimaryKey val name: String,
    val lat: Double,
    val lon: Double
) {
    companion object {
        internal val unknownCity = City("", 0.0, 0.0)

        internal fun fromLocation(lat: Double, lon: Double) =
            City(
                name = String.format("lat: %.2f lon: %.2f", lat, lon),
                lat, lon,
            )
    }
}

data class CityWeather(
    val name: String,
    val temperature: String,
    val weather: Weather,
    val weatherDescription: String,
)

typealias Cities = List<CityWeather>

val bigCitiesInUkraine = listOf(
    "Dnipro", "Kiev", "Kharkiv", "Kryvyi Rih",
    "Odessa", "Poltava", "Lviv", "Zaporizhzhia",
)
