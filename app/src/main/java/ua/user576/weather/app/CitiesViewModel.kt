package ua.user576.weather.app

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import ua.user576.weather.app.entities.Cities
import javax.inject.Inject

@HiltViewModel
class CitiesViewModel @Inject constructor(val useCase: GetCitiesWeatherUseCase): ViewModel() {
    private val flow: MutableStateFlow<Cities> = MutableStateFlow(emptyList())
    val cities: StateFlow<Cities> = flow.asStateFlow()

    init {
        viewModelScope.launch(Dispatchers.IO) { loadCities() }
    }

    private fun loadCities() {
        flow.value = useCase.getBigCitiesWithWeather()
    }
}