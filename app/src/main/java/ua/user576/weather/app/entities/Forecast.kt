package ua.user576.weather.app.entities

data class Forecast(
    val time: String,
    val day: String,
    val weather: Weather,
    val weatherDescription: String,
    val temperature: String,
)

typealias Forecasts = List<Forecast>
