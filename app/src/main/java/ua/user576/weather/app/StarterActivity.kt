package ua.user576.weather.app

import android.Manifest.permission.ACCESS_COARSE_LOCATION
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.net.ConnectivityManager
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.core.app.ActivityCompat
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices

class StarterActivity : Activity() {
    private lateinit var connectivityManager: ConnectivityManager
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (connectivityManager.activeNetwork == null) {
            setContentView(R.layout.internet_not_available_screen)
            findViewById<ImageView>(R.id.refresh_icon)
                .setOnClickListener { retry() }
        } else {
            checkLocationPermissionAndStarMainActivity()
        }
    }

    private fun retry() {
        findViewById<LinearLayout>(R.id.linear_layout)
            .visibility = View.INVISIBLE

        // delay for 1 second to show that icon is clickable
        Handler(Looper.getMainLooper()).postDelayed(::checkNetwork, 1000)
    }

    private fun checkNetwork() {
        if (connectivityManager.activeNetwork == null) {
            findViewById<LinearLayout>(R.id.linear_layout)
                .visibility = View.VISIBLE
        } else {
            checkLocationPermissionAndStarMainActivity()
        }
    }

    private fun checkLocationPermissionAndStarMainActivity() {
        val result = checkSelfPermission(ACCESS_COARSE_LOCATION)
        if (result == PackageManager.PERMISSION_GRANTED) {
            accessLocation()
        } else {
            ActivityCompat.requestPermissions(this, arrayOf(ACCESS_COARSE_LOCATION), 1)
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (grantResults.first() == PackageManager.PERMISSION_GRANTED) {
            accessLocation()
        } else {
            MainActivity.startWhenPermissionDenied(this)
        }
    }

    private fun accessLocation() {
        val result = checkSelfPermission(ACCESS_COARSE_LOCATION)
        if (result == PackageManager.PERMISSION_GRANTED) {
            val client: FusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)
            client.lastLocation
                .addOnSuccessListener { location: Location? ->
                    MainActivity.startWithLocation(this, location)
                    this.finish()
                }
        }
    }
}
