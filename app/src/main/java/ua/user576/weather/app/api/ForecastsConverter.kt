package ua.user576.weather.app.api

import ua.user576.weather.app.entities.Forecast
import ua.user576.weather.app.entities.weatherFromString
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

class ForecastsConverter {
    private val timeFormatter = SimpleDateFormat("HH:mm", Locale.getDefault())
    private val dayFormatter = SimpleDateFormat("EEE", Locale.ENGLISH)

    fun convert(arrayElement: ArrayElement): Forecast {
        val date = Date(arrayElement.dt * 1000)
        val strWeather = arrayElement.weather.first()["main"].toString()
        val tempKelvin = arrayElement.main.temp
        val tempCelsius = tempKelvin - 273.15

        return Forecast(
            time = timeFormatter.format(date),
            day = dayFormatter.format(date),
            weather = weatherFromString(strWeather),
            weatherDescription = arrayElement.weather.first()["description"].toString(),
            temperature = String.format("%2.1f", tempCelsius)
        )
    }
}
