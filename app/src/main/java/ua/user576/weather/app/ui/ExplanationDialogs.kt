package ua.user576.weather.app.ui

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.material.AlertDialog
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import ua.user576.weather.app.entities.City

@Composable
fun ExtrasIsNullExplanationDialog() {
    ExplanationDialog(title = "Extras is NULL") {}
}

@Composable
fun PermissionDeniedExplanationDialog() {
    ExplanationDialog(title = "Permission denied") {
        Column {
            Text(
                    "It seems like you denied access to your location, "
                    + "so the app has no ability to automatically resolve your city."
            )
            Spacer(Modifier.height(20.dp))
            Text(
                "Access location permission is not mandatory.",
            )
            Spacer(Modifier.height(20.dp))
            Text(
                "Select your city manually.",
            )
        }
    }
}

@Composable
fun LocationIsNullExplanationDialog() {
    ExplanationDialog(title = "Location is NULL") {
        Column {
            Text(
                "Location is NULL. It's typically happens when location is disabled in settings",
            )
            Spacer(Modifier.height(20.dp))
            Text(
                "By the way, you can select your city on City Selection Screen",
            )
        }
    }
}

@Composable
fun ExplanationDialog(title: String, text: @Composable () -> Unit) {
    val isDialogOpened = remember { mutableStateOf(true) }

    WeatherApp(starterPage = Page.CitySelection, starterCity = City.unknownCity)

    if (isDialogOpened.value) {
        AlertDialog(
            title = { Text(title) },
            text = text,
            confirmButton = { Button(onClick = { isDialogOpened.value = false }) {
                Text("Ok")
            }
            },
            onDismissRequest = { isDialogOpened.value = false }
        )
    }
}
