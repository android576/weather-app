package ua.user576.weather.app.api

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

data class Main(
    val temp: Double,
)

data class ArrayElement(
    val dt: Long,
    val main: Main,
    val weather: List<Map<String, Any>>
)

data class ForecastResponse(
    val cod: String,
    val list: List<ArrayElement>,
)

interface ForecastsService {
    @GET("data/2.5/forecast")
    fun get(
        @Query("lat") lat: Double,
        @Query("lon") lon: Double,
        @Query("appid") apiKey: String,
        @Query("cnt") cnt: Int = 40,
    ): Call<ForecastResponse>
}
