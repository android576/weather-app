package ua.user576.weather.app.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import ua.user576.weather.app.entities.City

@Dao
interface CityDao {
    @Query("SELECT * FROM city WHERE name=:name")
    fun get(name: String): City

    @Query("SELECT * FROM city")
    fun getAll(): List<City>

    @Insert
    fun insert(city: City)
}
