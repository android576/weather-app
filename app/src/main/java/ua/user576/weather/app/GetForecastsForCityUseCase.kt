package ua.user576.weather.app

import ua.user576.weather.app.api.ForecastsRepository
import ua.user576.weather.app.entities.Forecasts
import javax.inject.Inject

class GetForecastsForCityUseCase @Inject constructor(val citiesRepository: CitiesRepository) {
    private val forecastsRepository = ForecastsRepository()

    fun getForecasts(cityName: String): Forecasts {
        val coordinates = citiesRepository.getCoordinates(cityName)
        return forecastsRepository.get(coordinates.lat, coordinates.lon)
    }
}
