package ua.user576.weather.app.ui

import androidx.compose.foundation.layout.*
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import ua.user576.weather.app.entities.Forecast
import ua.user576.weather.app.entities.Weather
import ua.user576.weather.app.ui.theme.WeatherAppTypography

@Composable
fun ForecastItem(modifier: Modifier = Modifier, forecast: Forecast) {
    Row(
        modifier
            .padding(20.dp)
            .fillMaxWidth(),
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.Center
    ) {
        Column(horizontalAlignment = Alignment.CenterHorizontally) {
            Text(text = forecast.time, style = WeatherAppTypography.subtitle1)
            Text(text = forecast.day)
        }

        Spacer(Modifier.width(20.dp))
        Column(horizontalAlignment = Alignment.CenterHorizontally) {
            WeatherIcon(forecast.weather, forecast.weatherDescription)
            Text(text = forecast.weatherDescription)
        }

        Spacer(Modifier.width(20.dp))
        TemperatureLabel(temperature = forecast.temperature)
    }
}

@Preview
@Composable
fun Preview() {
    ForecastItem(
        forecast = Forecast(
            time ="12:00", day = "Sat",
            weather = Weather.Clear, weatherDescription = "clear sky",
            temperature = "23,1",
        )
    )
}
