package ua.user576.weather.app.ui

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Icon
import androidx.compose.material.Surface
import androidx.compose.material.TextField
import androidx.compose.material.TextFieldDefaults
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Search
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import ua.user576.weather.app.CitiesViewModel
import ua.user576.weather.app.entities.CityWeather
import ua.user576.weather.app.entities.Weather
import ua.user576.weather.app.entities.bigCitiesInUkraine
import ua.user576.weather.app.ui.theme.Shapes
import ua.user576.weather.app.ui.theme.WeatherAppTheme
import ua.user576.weather.app.ui.theme.WeatherAppTypography

@Composable
fun CityItem(modifier: Modifier = Modifier, city: CityWeather, onClick: () -> Unit) {
    Box(modifier.padding(5.dp)) {
        Box(
            Modifier
                .border(BorderStroke(1.dp, Color.White), Shapes.large)
                .clickable { onClick() }
        ) {
            Row(
                Modifier
                    .padding(15.dp)
                    .fillMaxWidth(),
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.Center
            ) {
                TemperatureLabel(temperature = city.temperature)
                Spacer(Modifier.width(20.dp))
                WeatherIcon(weather = city.weather, weatherDescription = city.weatherDescription)
                Spacer(Modifier.width(20.dp))
                Column {
                    Text(city.name, style = WeatherAppTypography.subtitle1)
                    Text(city.weatherDescription, style = WeatherAppTypography.body2)
                }
            }
        }
    }
}

@Composable
fun CitySelectionScreen(onCityChange: (String) -> Unit, viewModel: CitiesViewModel = viewModel()) {
    val cities by viewModel.cities.collectAsState()
    CitiesList(
        cities,
        onChange = { selectedCity ->
            onCityChange(selectedCity.name)
        }
    )
}

@Composable
fun CitiesList(cities: List<CityWeather>, onChange: (CityWeather) -> Unit) {
    val searchPattern = remember { mutableStateOf("") }

    val filteredCities: List<CityWeather> = cities.filter { cityWeather ->
        cityWeather.name.startsWith(searchPattern.value, ignoreCase = true)
    }

    Column {
        Text("City Selection Screen", Modifier.padding(10.dp), style = WeatherAppTypography.h1)
        Spacer(Modifier.height(10.dp))

        if (cities.isEmpty()) {
            Text("Loading cities...", style = WeatherAppTypography.h1)
        } else {
            Row(Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.Center) {
                TextField(
                    value = searchPattern.value,
                    onValueChange = { newPattern -> searchPattern.value = newPattern },
                    colors = TextFieldDefaults.textFieldColors(
                        cursorColor = Color.White, focusedIndicatorColor = Color.White,
                        trailingIconColor = Color.White
                    ),
                    textStyle = WeatherAppTypography.body1,
                    placeholder = { Text("Search your city", style = WeatherAppTypography.body1) },
                    trailingIcon = {
                        Icon(
                            imageVector = Icons.Default.Search,
                            contentDescription = "search",
                        )
                    }
                )
            }
            Spacer(Modifier.height(10.dp))
            if (filteredCities.isEmpty()) {
                Text("Nothing satisfies your search pattern", style = WeatherAppTypography.h1)
            } else {
                LazyColumn {
                    items(filteredCities) { city ->
                        CityItem(
                            city = city,
                            onClick = { onChange(city) }
                        )
                    }
                }
            }
        }
    }
}

@Preview
@Composable
fun PreviewAllCities() {
    WeatherAppTheme {
        Surface {
            CitiesList(
                cities = bigCitiesInUkraine.map { cityName ->
                    CityWeather(cityName, "21,0", Weather.Clouds, "broken clouds")
                },
                onChange = {}
            )
        }
    }
}

@Preview
@Composable
fun PreviewCitiesWithDifferentWeather() {
    WeatherAppTheme {
        Surface {
            CitiesList(
                cities = listOf(
                    CityWeather("Dnipro", "24,9", Weather.Clouds, "broken clouds"),
                    CityWeather("Kiyv", "21,0", Weather.Clouds, "few clouds"),
                    CityWeather("Kharkiv", "22,6", Weather.Rain, "light rain"),
                ),
                onChange = {}
            )
        }
    }
}
