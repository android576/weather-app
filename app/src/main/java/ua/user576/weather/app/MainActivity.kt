package ua.user576.weather.app

import android.content.Context
import android.content.Intent
import android.location.Location
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.runtime.Composable
import dagger.hilt.android.AndroidEntryPoint
import ua.user576.weather.app.entities.City
import ua.user576.weather.app.ui.ExtrasIsNullExplanationDialog
import ua.user576.weather.app.ui.LocationIsNullExplanationDialog
import ua.user576.weather.app.ui.Page
import ua.user576.weather.app.ui.PermissionDeniedExplanationDialog
import ua.user576.weather.app.ui.theme.WeatherAppTheme
import ua.user576.weather.app.ui.WeatherApp

@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            WeatherAppTheme {
                ComponentSelector(intent.extras)
            }
        }
    }

    companion object {
        internal const val KEY_PERMISSION_DENIED = "permission_denied"
        internal const val KEY_LOCATION_IS_NULL = "is_location_null"
        internal const val KEY_LAT = "lat"
        internal const val KEY_LON = "lon"

        internal fun startWithLocation(context: Context, location: Location?) {
            val intent = Intent(context, MainActivity::class.java)
            intent.putExtra(KEY_LOCATION_IS_NULL, location == null)
            if (location != null) {
                intent.putExtra(KEY_LAT, location.latitude)
                intent.putExtra(KEY_LON, location.longitude)
            }
            context.startActivity(intent)
        }

        internal fun startWhenPermissionDenied(context: Context) {
            val intent = Intent(context, MainActivity::class.java)
            intent.putExtra(KEY_PERMISSION_DENIED, true)
            context.startActivity(intent)
        }
    }
}

@Composable
fun ComponentSelector(extras: Bundle?) {
    when {
        extras == null -> ExtrasIsNullExplanationDialog()
        extras.getBoolean(MainActivity.KEY_PERMISSION_DENIED) -> PermissionDeniedExplanationDialog()
        extras.getBoolean(MainActivity.KEY_LOCATION_IS_NULL) -> LocationIsNullExplanationDialog()
        else -> {
            WeatherApp(
                starterPage = Page.Forecast,
                starterCity = City.fromLocation(
                    lat = extras.getDouble(MainActivity.KEY_LAT),
                    lon = extras.getDouble(MainActivity.KEY_LON)
                )
            )
        }
    }
}
