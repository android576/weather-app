package ua.user576.weather.app.api

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query
import ua.user576.weather.app.entities.City

typealias GeoDirectEndpointResponse = List<City>

interface CitiesService {
    @GET("geo/1.0/direct")
    fun get(
        @Query("q") q: String,
        @Query("appid") apiKey: String,
        @Query("limit") limit: Int = 1,
    ): Call<GeoDirectEndpointResponse>
}
