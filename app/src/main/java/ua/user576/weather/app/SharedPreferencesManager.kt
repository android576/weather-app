package ua.user576.weather.app

import android.content.Context
import android.content.SharedPreferences
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject

class SharedPreferencesManager @Inject constructor(@ApplicationContext context: Context) {
    private val sharedPreferences: SharedPreferences

    init {
        sharedPreferences = context.getSharedPreferences("app_prefs", Context.MODE_PRIVATE)
    }

    var wasCitiesLoaded: Boolean
        get() = sharedPreferences.getBoolean(KEY, false)
        set(value) {
            sharedPreferences.edit()
                .putBoolean(KEY, value)
                .apply()
        }

    companion object {
        private const val KEY = "was_cities_loaded"
    }
}
