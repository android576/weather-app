package ua.user576.weather.app.api

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query
import ua.user576.weather.app.entities.City

typealias ReverseGeocodingApiResponseService = List<City>

interface ReverseGeocodingService {
    @GET("geo/1.0/reverse")
    fun get(
        @Query("lat") lat: Double,
        @Query("lon") lon: Double,
        @Query("appid") apiKey: String,
    ): Call<ReverseGeocodingApiResponseService>
}
