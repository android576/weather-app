package ua.user576.weather.app.entities

enum class Weather {
    Clear, Rain, Clouds, Unknown
}

fun weatherFromString(str: String): Weather =
    when(str) {
        "Clear" -> Weather.Clear
        "Rain" -> Weather.Rain
        "Clouds" -> Weather.Clouds
        else -> Weather.Unknown
    }
