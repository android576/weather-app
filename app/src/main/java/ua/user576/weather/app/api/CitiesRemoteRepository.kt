package ua.user576.weather.app.api

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import ua.user576.weather.app.entities.City
import ua.user576.weather.app.entities.bigCitiesInUkraine

class CitiesRemoteRepository {
    private val citiesService: CitiesService
    private val reverseGeocodingService: ReverseGeocodingService

    init {
        val retrofit = Retrofit.Builder()
            .baseUrl("https://api.openweathermap.org/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        citiesService = retrofit.create(CitiesService::class.java)
        reverseGeocodingService = retrofit.create(ReverseGeocodingService::class.java)
    }
    fun getBigCitiesCoordinates(): List<City> =
        bigCitiesInUkraine.map { cityName -> getCoordinates(cityName)}

    fun getCityByCoordinates(lat: Double, lon: Double): City {
        val call = reverseGeocodingService.get(lat, lon, apiKey)
        val response = call.execute().body()
        return response?.first() ?: City("response is null", 0.0, 0.0)
    }

    private fun getCoordinates(cityName: String): City {
        val call = citiesService.get(cityName, apiKey)
        val response: GeoDirectEndpointResponse? = call.execute().body()
        return response?.first() ?: City("response is null", 0.0, 0.0)
    }
}
