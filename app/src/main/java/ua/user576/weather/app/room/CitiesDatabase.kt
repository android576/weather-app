package ua.user576.weather.app.room

import androidx.room.Database
import androidx.room.RoomDatabase
import ua.user576.weather.app.entities.City

@Database(entities = [City::class], version = 1, exportSchema = false)
abstract class CitiesDatabase: RoomDatabase() {
    abstract fun cityDao(): CityDao
}
