package ua.user576.weather.app

import ua.user576.weather.app.api.ForecastsRepository
import ua.user576.weather.app.entities.CityWeather
import javax.inject.Inject

class GetCitiesWeatherUseCase @Inject constructor(val citiesRepository: CitiesRepository) {
    private val forecastsRepository = ForecastsRepository()

    internal fun getBigCitiesWithWeather(): List<CityWeather> =
        citiesRepository.getBigCities()
            .map { coordinates ->
                val forecasts = forecastsRepository.get(coordinates.lat, coordinates.lon, 1)
                val forecast = forecasts.first()
                CityWeather(
                    name = coordinates.name,
                    temperature = forecast.temperature,
                    weather = forecast.weather,
                    weatherDescription = forecast.weatherDescription
                )
            }
}
