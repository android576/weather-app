package ua.user576.weather.app.ui

import androidx.compose.foundation.Image
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.res.painterResource
import ua.user576.weather.app.R
import ua.user576.weather.app.entities.Weather
import ua.user576.weather.app.ui.theme.WeatherAppTypography

@Composable
fun WeatherIcon(weather: Weather, weatherDescription: String) {
    Image(
        contentDescription = weatherDescription,
        painter = painterResource(
            id = when(weather) {
                Weather.Clear -> R.drawable.sunny
                Weather.Rain -> R.drawable.rain
                Weather.Clouds -> R.drawable.cloud
                else -> R.drawable.location_city
            }
        ),
    )
}

@Composable
fun TemperatureLabel(temperature: String) {
    Text("$temperature°C", style = WeatherAppTypography.subtitle1)
}
