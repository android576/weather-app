package ua.user576.weather.app.api

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import ua.user576.weather.app.entities.Forecast
import ua.user576.weather.app.entities.Forecasts
import ua.user576.weather.app.entities.Weather

class ForecastsRepository {
    private val service: ForecastsService
    private val forecastsConverter: ForecastsConverter

    init {
        val retrofit = Retrofit.Builder()
            .baseUrl("https://api.openweathermap.org/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        service = retrofit.create(ForecastsService::class.java)
        forecastsConverter = ForecastsConverter()
    }

    fun get(lat: Double, lon: Double, count: Int = 40): Forecasts {
        val call = service.get(lat, lon, apiKey, count)
        val response: ForecastResponse? = call.execute().body()
        return if (response == null) {
            listOf(Forecast("null", "null", Weather.Unknown, "null", "null"))
        } else {
            response.list.map { arrayElement ->
                forecastsConverter.convert(arrayElement)
            }
        }
    }
}
