package ua.user576.weather.app

import ua.user576.weather.app.api.CitiesRemoteRepository
import ua.user576.weather.app.entities.City
import ua.user576.weather.app.room.CityDao
import javax.inject.Inject

class CitiesRepository @Inject constructor(
    private val sharedPreferencesManager: SharedPreferencesManager,
    private val cityDao: CityDao
) {
    private val remoteRepository = CitiesRemoteRepository()

    fun getBigCities(): List<City> {
        if (sharedPreferencesManager.wasCitiesLoaded) {
            return cityDao.getAll()
        } else {
            val cities = remoteRepository.getBigCitiesCoordinates()
            cities.forEach { city -> cityDao.insert(city) }
            sharedPreferencesManager.wasCitiesLoaded = true
            return  cities
        }
    }

    fun getCoordinates(cityName: String): City =
        cityDao.get(cityName)
}
