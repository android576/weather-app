package ua.user576.weather.app.ui

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import ua.user576.weather.app.WeatherViewModel
import androidx.compose.runtime.getValue
import androidx.compose.runtime.collectAsState
import androidx.compose.foundation.lazy.items
import ua.user576.weather.app.ui.theme.WeatherAppTypography

@Composable
fun ForecastsScreen(viewModel: WeatherViewModel) {
    val cityName by viewModel.cityName.collectAsState()
    val forecasts by viewModel.forecasts.collectAsState()

    Column {
        Text(
            cityName, Modifier.padding(20.dp),
            fontSize = 52.sp, fontWeight = FontWeight(600)
        )
        if (forecasts.isEmpty()) {
            Text("Loading forecasts...", style = WeatherAppTypography.h1)
        } else {
            LazyColumn {
                items(forecasts) { forecast ->
                    ForecastItem(forecast = forecast)
                }
            }
        }
    }
}
