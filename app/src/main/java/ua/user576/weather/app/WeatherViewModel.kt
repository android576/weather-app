package ua.user576.weather.app

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import ua.user576.weather.app.api.CitiesRemoteRepository
import ua.user576.weather.app.api.ForecastsRepository
import ua.user576.weather.app.entities.City
import ua.user576.weather.app.entities.Forecasts
import javax.inject.Inject

@HiltViewModel
class WeatherViewModel @Inject constructor(val useCase: GetForecastsForCityUseCase): ViewModel() {
    private val cityFlow: MutableStateFlow<String> = MutableStateFlow("City is not known yet")
    val cityName: StateFlow<String> = cityFlow.asStateFlow()
    private val forecastsFlow = MutableStateFlow<Forecasts>(emptyList())
    val forecasts: StateFlow<Forecasts> = forecastsFlow.asStateFlow()

    private val citiesRemoteRepository = CitiesRemoteRepository()
    private val forecastsRepository = ForecastsRepository()

    internal fun changeCity(cityName: String) {
        cityFlow.value = cityName
        forecastsFlow.value = emptyList()
        loadForecasts(cityName)
    }

    internal fun setStartCity(starterCity: City) {
        cityFlow.value = starterCity.name

        viewModelScope.launch(Dispatchers.IO) {
            val city = citiesRemoteRepository.getCityByCoordinates(starterCity.lat, starterCity.lon)
            cityFlow.value = city.name
            forecastsFlow.value = forecastsRepository.get(starterCity.lat, starterCity.lon)
        }
    }

    private fun loadForecasts(cityName: String) {
        viewModelScope.launch(Dispatchers.IO) {
            forecastsFlow.value = useCase.getForecasts(cityName)
        }
    }
}
