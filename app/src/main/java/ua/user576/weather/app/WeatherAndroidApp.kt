package ua.user576.weather.app

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class WeatherAndroidApp: Application() {}
