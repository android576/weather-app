package ua.user576.weather.app.ui.theme

import androidx.compose.material.MaterialTheme
import androidx.compose.material.darkColors
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color

private val colorPalette = darkColors(
    primary = Color(0xFF2f356a),
    primaryVariant = Purple700,
    secondary = Teal200,
    surface = BackgroundColor,
    background = BackgroundColor,

/* Other default colors to override
background = Color.White,
surface = Color.White,
onPrimary = Color.White,
onSecondary = Color.Black,
onBackground = Color.Black,
onSurface = Color.Black,
*/
)

@Composable
fun WeatherAppTheme(content: @Composable () -> Unit) {
    MaterialTheme(
        colors = colorPalette,
        typography = WeatherAppTypography,
        shapes = Shapes,
        content = content
    )
}