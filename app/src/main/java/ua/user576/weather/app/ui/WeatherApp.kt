package ua.user576.weather.app.ui

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.padding
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import ua.user576.weather.app.ui.theme.WeatherAppTheme
import ua.user576.weather.app.R
import ua.user576.weather.app.WeatherViewModel
import ua.user576.weather.app.entities.City
import ua.user576.weather.app.ui.theme.SelectedBottomItemLabelColor
import ua.user576.weather.app.ui.theme.WeatherAppTypography

enum class Page {
    Forecast, CitySelection
}

@Composable
fun BottomItemLabel(label: String) {
    Text(label, color = SelectedBottomItemLabelColor)
}

@Composable
fun WeatherApp(starterPage: Page, starterCity: City) {
    val weatherViewModel: WeatherViewModel = viewModel()
    if (starterCity != City.unknownCity) {
        weatherViewModel.setStartCity(starterCity)
    }

    var page by remember { mutableStateOf(starterPage) }

    fun onCityChanged(cityName: String) {
        weatherViewModel.changeCity(cityName)
        page = Page.Forecast
    }

    Scaffold(
        content = { paddingValues ->
            Surface(Modifier.padding(paddingValues)) {
                Box(Modifier.padding(20.dp)) {
                    when(page) {
                        Page.Forecast -> ForecastsScreen(weatherViewModel)
                        Page.CitySelection -> CitySelectionScreen(::onCityChanged)
                    }
                }
            }
        },
        bottomBar = {
            BottomNavigation {
                BottomNavigationItem(
                    selected = page == Page.Forecast,
                    onClick = { page = Page.Forecast },
                    icon = {
                        Text(
                            "Forecast",
                            style = WeatherAppTypography.button,
                            color = if (page == Page.Forecast)
                                SelectedBottomItemLabelColor
                            else
                                Color.White
                        )
                    },
                    label = { BottomItemLabel("for every 3 hours") },
                    alwaysShowLabel = false
                )
                BottomNavigationItem(
                    selected = page == Page.CitySelection,
                    onClick = { page = Page.CitySelection },
                    label = { BottomItemLabel("City selection") },
                    icon = {
                        Image(
                            painter = painterResource(id = R.drawable.location_city),
                            contentDescription = "location city"
                        )
                    },
                    alwaysShowLabel = false
                )
            }
        })
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    WeatherAppTheme {
        WeatherApp(Page.Forecast, starterCity = City.unknownCity)
    }
}
