# Weather App

The Android app that fetches data from [Open Weather API](https://openweathermap.org/api) and displays it on a screen.

Jetpack Compose, MVVM, Kotlin Coroutines, Flow. Retrofit was used for data fetching. Hilt for DI.

Cities coordinates are loaded once and then stored in local SQLite database. Room is used for data persistence.

# Task

Using [Open Weather API](https://openweathermap.org/api) provide:
1) ability to view forecasts;
2) city selection and automatic location recognition.

# Result

[YouTube Video User DENIES access to location](https://youtube.com/shorts/Z1FpyHrht1k?feature=share)

[YouTube Video User ALLOWS location access](https://youtube.com/shorts/GKwZVLugQW0?feature=share)
